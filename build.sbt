ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "kps",
    idePackagePrefix := Some("com.gitlab.torinrakes.kps"),
    libraryDependencies := Seq (
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
      "org.slf4j" % "slf4j-simple" % "2.0.3"
    )
  )
