package com.gitlab.torinrakes.kps

package object time {
  implicit class TimeSequence[T, R <: Ordered[R]](list: List[(R, R, T)]) {
    override def toString: String = list.toString

    /**
     * adds new interval while preserving order; raises exception if interval overlaps existing
     */
    def append(from: R, to: R, value: T): TimeSequence[T, R] = {
      val slice1 = list.filter((tuple: (R, R, T)) => tuple._2 <= from)
      val slice2 = list.filter((tuple: (R, R, T)) => tuple._1 >= to)
      if (slice1.length + slice2.length != list.length) {
        throw new IllegalArgumentException("Could not perform slice")
      }
      mergeSlices(from, to, value, slice1, slice2)
    }

    /**
     * returns value at specified point
     */
    def get(index: R): Option[T] = list.find((tuple: (R, R, T)) => tuple._1 <= index && tuple._2 > index)
      .map(_._3)

    /**
     * finds first interval that has specified value
     */
    def find(value: T): Option[(R, R)] = list.find(_._3 == value)
      .map((tuple: (R, R, T)) => (tuple._1, tuple._2))

    /**
     * returns subsequence
     */
    def between(from: R, to: R): TimeSequence[T, R] =
      list.filter(contains(from, to))
        .map(shrink(from, to))

    private def mergeSlices(from: R, to: R, value: T, slice1: List[(R, R, T)], slice2: List[(R, R, T)]): List[(R, R, T)] =
      if (slice1.isEmpty && slice2.isEmpty)
        List((from, to, value))
      else if (slice1.isEmpty)
        slice2.prepended((from, to, value))
      else if (slice2.isEmpty)
        slice1.appended((from, to, value))
      else
        slice1.appended((from, to, value)).appendedAll(slice2)

    private def shrink(from: R, to: R): ((R, R, T)) => (R, R, T) = (tuple: (R, R, T)) =>
      if ((tuple._1 >= from) && (tuple._2 <= to))
        tuple
      else if ((tuple._1 <= from) && (tuple._2 >= from))
        (from, tuple._2, tuple._3)
      else
        (tuple._1, to, tuple._3)

    private def contains(from: R, to: R): ((R, R, T)) => Boolean = (tuple: (R, R, T)) =>
      (tuple._1 >= from) && (tuple._2 <= to) ||
        (tuple._1 <= from) && (tuple._2 > from) ||
        (tuple._1 < to) && (tuple._2 >= to)
  }

  /**
   * used to create a new sequence
   */
  def emptySequence[T, R <: Ordered[R]]: TimeSequence[T, R] = List()
}
