package com.gitlab.torinrakes.kps
package time

import com.typesafe.scalalogging.Logger

object Main {
  val logger: Logger = Logger(Main.getClass)

  def main(args: Array[String]): Unit = {
    val empty: TimeSequence[Int, BigDecimal] = emptySequence
    // (1, 2, 3), (2, 3, 4), (6, 8, 3)
    val seq = empty.append(6, 8, 3)
      .append(1, 2, 3)
      .append(2, 3, 4)
    // None
    logger.info("{}", seq.between(4, 5))
    // (1, 2)
    logger.info("{}", seq.find(3))
    // 3
    logger.info("{}", seq.get(7))
  }
}