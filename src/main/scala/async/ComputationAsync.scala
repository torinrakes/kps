package com.gitlab.torinrakes.kps
package async

import scala.concurrent.{ExecutionContext, Future}

/**
 * @param data actual computation
 * @tparam IN  input type; type of function argument or Unit in case no argument is needed
 * @tparam OUT output type; type of the result of computation
 */
class ComputationAsync[IN, OUT](val data: IN => Future[OUT]) {

  /**
   * consumes computation-function arguments and returns computation-value
   */
  def consume(computation: ReadyToEvaluate[IN])(implicit ec: ExecutionContext): ComputationAsync[Unit, OUT] =
    new ComputationAsync[Unit, OUT]((_: Unit) => computation.eval()
      .flatMap((in: IN) => data(in)))

  /**
   * chains computations
   */
  def `then`[NEW_OUT](function: OUT => NEW_OUT)(implicit ec: ExecutionContext): ComputationAsync[IN, NEW_OUT] =
    new ComputationAsync[IN, NEW_OUT]((in: IN) => Future.delegate(data(in))
      .map((out: OUT) => function(out)))

  /**
   * branches computation based on predicate value
   */
  def ifThenElse[OUT1, OUT2](predicate: OUT => Boolean, thenBranch: OUT => OUT1, elseBranch: OUT => OUT2)(implicit ec: ExecutionContext): ComputationAsync[IN, Either[OUT1, OUT2]] =
    new ComputationAsync[IN, Either[OUT1, OUT2]]((in: IN) => Future.delegate(data(in))
      .flatMap(out => Future(predicate(out))
        .flatMap((bool: Boolean) =>
          if (bool)
            Future(thenBranch(out))
              .map((out1: OUT1) => Left(out1))
          else
            Future(elseBranch(out))
              .map((out2: OUT2) => Right(out2)))))

  /**
   * joins two computation into one; input and output types are combined in tuples
   */
  def join[IN1, OUT1](another: ComputationAsync[IN1, OUT1])(implicit ec: ExecutionContext): ComputationAsync[(IN, IN1), (OUT, OUT1)] =
    new ComputationAsync[(IN, IN1), (OUT, OUT1)]((tuple: (IN, IN1)) => Future.delegate(data(tuple._1))
      .zipWith(Future.delegate(another.data(tuple._2)))((out: OUT, out1: OUT1) => (out, out1)))

  override def toString = s"ComputationAsync($data)"
}
