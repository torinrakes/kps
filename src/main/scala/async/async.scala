package com.gitlab.torinrakes.kps

import com.typesafe.scalalogging.Logger

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.implicitConversions

package object async {
  val logger: Logger = Logger(Main.getClass)

  /**
   * creates computation from pure value
   */
  implicit def fromValue[T](value: T)(implicit ec: ExecutionContext): ComputationAsync[Unit, T] =
    new ComputationAsync[Unit, T](_ => Future.successful(value))

  /**
   * creates computation from anonymous function
   */
  implicit def fromFunction[IN, OUT](function: IN => OUT)(implicit ec: ExecutionContext): ComputationAsync[IN, OUT] =
    new ComputationAsync[IN, OUT]((in: IN) => Future {
      function(in)
    })

  /**
   * extension methods for computations that are ready to be evaluated
   */
  implicit class ReadyToEvaluate[T](computation: ComputationAsync[Unit, T]) {
    def eval()(implicit ec: ExecutionContext): Future[T] = computation.data(())

    def evalAndRun()(implicit ec: ExecutionContext): Unit = {
      logger.info("Start evaluation!")
      val result = computation.eval()
      Await.ready(result, Duration.Inf) // wait indefinitely
      val string = result.value
        .get
        .fold((throwable: Throwable) => s"Failed to evaluate: $throwable", (t: T) => s"Finished evaluation: \"$t\"")
      logger.info(string)
    }
  }

  /**
   * extension methods for computations that contain Unit (void) as input type
   */
  implicit class LeftAndUnit[T, OUT](computation: ComputationAsync[(T, Unit), OUT])(implicit ec: ExecutionContext) {
    def flat(): ComputationAsync[T, OUT] = new ComputationAsync[T, OUT](t => Future {
      computation.data((t, ()))
    }.flatMap(f => f))
  }
}
