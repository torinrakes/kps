package com.gitlab.torinrakes.kps
package async

import com.typesafe.scalalogging.Logger

import scala.concurrent.ExecutionContext.Implicits.global

object Main {
  val logger: Logger = Logger(Main.getClass)

  def main(args: Array[String]): Unit = {
    example1.evalAndRun()
    example2.evalAndRun()
  }

  private val example1 = {
    logger.info("Start describing example1")
    // int > 0 ? throw : "Good value!"
    val f1 = (int: Int) => {
      logger.info("Start f1")
      val result = if (int > 0) {
        throw new IllegalArgumentException(s"Int must not be more than zero, but received $int")
      } else {
        "Good value!"
      }
      logger.info("Finish f1 with {}", result)
      result
    }
    // s => s.contains("Good") ? throw : "Not so good!"
    val f2 = (as: String) => {
      logger.info("Start f2")
      val result = if (as.contains("Good")) {
        throw new IllegalArgumentException(s"BAD!!!")
      } else {
        s"Not so good!"
      }
      logger.info("Finish f2 with {}", result)
      result
    }
    // f2 ∘ f1
    val f3 = f1.`then`(f2)
    // 5
    val c1 = fromValue(5)
    // (f2 ∘ f1) c1
    val c2 = f3.consume(c1)
    logger.info("Finish describing example1")
    c2
  }

  private val example2 = {
    logger.info("Start describing example2")
    // 4
    val c1 = fromValue(4)
    // 5
    val c2 = fromValue(5)
    // 6
    val c3 = fromValue(6)
    // (int1, int2) => int1 + int2
    val f1 = (tuple: (Int, Int)) => {
      logger.info("start summing {} and {}", tuple._1, tuple._2)
      val result = tuple._1 + tuple._2
      logger.info("result is {}", result)
      result
    }
    // (int1, int2) => int1 * int2
    val f2 = (tuple: (Int, Int)) => {
      logger.info("start multiplying {} and {}", tuple._1, tuple._2)
      val result = tuple._1 * tuple._2
      logger.info("result is {}", result)
      result
    }
    // (c2, c3)
    val c4 = c2.join(c3).flat()
    // f2 c4
    val c5 = f2.consume(c4)
    // (c1, c5)
    val c6 = c1.join(c5).flat()
    // f1 c6
    val c7 = f1.consume(c6)
    // int => int > 40
    val f3 = (int: Int) => {
      logger.info("evaluating predicate on {}", int)
      val result = int > 40
      logger.info("result is {}", result)
      result
    }
    // int => "HELLO!"
    val f4 = (int: Int) => {
      logger.info("converting then branch from {}", int)
      val result = "HELLO!"
      logger.info("converted to {}", result)
      result
    }
    // int => int - 1
    val f5 = (int: Int) => {
      logger.info("converting else branch from {}", int)
      val result = int - 1
      logger.info("converted to {}", result)
      result
    }
    // (if f3 then f4 else f5) c7
    val c8 = c7.ifThenElse(f3, f4, f5)
    // (s, int) => String
    val f6 = (stringOrInt: Either[String, Int]) => {
      logger.info("continuing evaluation from {}", stringOrInt)
      val result = stringOrInt.fold(s => s, i => s"Computed: ${i}")
      logger.info("evaluated {}", result)
      result
    }
    // f6 ∘ c8
    val c9 = c8.`then`(f6)
    logger.info("Finish describing example2")
    c9
  }
}
