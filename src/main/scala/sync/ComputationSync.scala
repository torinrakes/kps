package com.gitlab.torinrakes.kps
package sync

import scala.util.Try

/**
 * @param data actual computation
 * @tparam IN  input type; type of function argument or Unit in case no argument is needed
 * @tparam OUT output type; type of the result of computation
 */
class ComputationSync[IN, OUT](val data: IN => Try[OUT]) {

  /**
   * consumes computation-function arguments and returns computation-value
   */
  def consume(computation: ReadyToEvaluate[IN]): ReadyComputationSync[OUT] =
    new ReadyComputationSync[OUT](computation.eval().flatMap(in => data.apply(in)))

  /**
   * chains computations
   */
  def `then`[NEW_OUT](function: OUT => Try[NEW_OUT]): ComputationSync[IN, NEW_OUT] =
    new ComputationSync[IN, NEW_OUT](in => data.apply(in)
      .flatMap(out => function.apply(out)))

  /**
   * branches computation based on predicate value
   */
  def ifThenElse[OUT1, OUT2](predicate: OUT => Try[Boolean], thenBranch: OUT => Try[OUT1], elseBranch: OUT => Try[OUT2]): ComputationSync[IN, Either[OUT1, OUT2]] =
    new ComputationSync[IN, Either[OUT1, OUT2]](in => data.apply(in)
      .flatMap(out => predicate.apply(out)
        .flatMap {
          case true => thenBranch.apply(out).map(Left(_))
          case false => elseBranch.apply(out).map(Right(_))
        }))

  /**
   * joins two computation into one; input and output types are combined in tuples
   */
  def join[IN1, OUT1](another: ComputationSync[IN1, OUT1]): ComputationSync[(IN, IN1), (OUT, OUT1)] =
    new ComputationSync[(IN, IN1), (OUT, OUT1)]((tuple: (IN, IN1)) => data.apply(tuple._1)
      .flatMap(out => another.data.apply(tuple._2)
        .map(out1 => (out, out1))))

  override def toString = s"ComputationSync($data)"
}
