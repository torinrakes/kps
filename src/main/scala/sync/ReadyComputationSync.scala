package com.gitlab.torinrakes.kps
package sync

import scala.util.Try

/**
 * methods for computations that are evaluated
 */
class ReadyComputationSync[OUT](val value: Try[OUT]) extends ComputationSync[Unit, OUT](_ => value) {
  /**
   * chains computations
   */
  override def `then`[NEW_OUT](function: OUT => Try[NEW_OUT]): ComputationSync[Unit, NEW_OUT] =
    new ReadyComputationSync[NEW_OUT](value.flatMap(function))

  /**
   * branches computation based on predicate value
   */
  override def ifThenElse[OUT1, OUT2](predicate: OUT => Try[Boolean], thenBranch: OUT => Try[OUT1], elseBranch: OUT => Try[OUT2]): ComputationSync[Unit, Either[OUT1, OUT2]] =
    new ReadyComputationSync[Either[OUT1, OUT2]](value
      .flatMap(out => predicate.apply(out)
        .flatMap {
          case true => thenBranch.apply(out).map(Left(_))
          case false => elseBranch.apply(out).map(Right(_))
        }))

  def eval(): Try[OUT] = value

  def evalAndRun(): Unit = {
    logger.info("Start evaluation!")
    val result = eval()
    val string = result.fold((throwable: Throwable) => s"Failed to evaluate: $throwable", (t: OUT) => s"Finished evaluation: \"$t\"")
    logger.info(string)
  }
}
