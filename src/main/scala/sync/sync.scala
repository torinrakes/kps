package com.gitlab.torinrakes.kps

import com.typesafe.scalalogging.Logger

import scala.language.implicitConversions
import scala.util.{Success, Try}

package object sync {
  val logger: Logger = Logger(Main.getClass)

  /**
   * creates computation from pure value
   */
  implicit def fromValue[T](value: T): ReadyComputationSync[T] = new ReadyComputationSync[T](Success(value))

  /**
   * wraps function body with try-catch block
   */
  implicit def wrap[IN, OUT](function: IN => OUT): IN => Try[OUT] = in => Try(function.apply(in))

  /**
   * creates computation from anonymous function
   */
  implicit def fromFunction[IN, OUT](function: IN => OUT): ComputationSync[IN, OUT] = new ComputationSync[IN, OUT](function)

  implicit class ReadyToEvaluate[T](computation: ComputationSync[Unit, T]) {
    def eval(): Try[T] = computation.data.apply()

    def evalAndRun(): Unit = {
      logger.info("Start evaluation!")
      val result = computation.eval()
      val string = result.fold((throwable: Throwable) => s"Failed to evaluate: $throwable", (t: T) => s"Finished evaluation: \"$t\"")
      logger.info(string)
    }
  }

  /**
   * extension methods for computations that contain Unit (void) as input type
   */
  implicit class LeftAndUnit[T, OUT](computation: ComputationSync[(T, Unit), OUT]) {
    def flat(): ComputationSync[T, OUT] = new ComputationSync[T, OUT](t => computation.data.apply((t, ())))
  }
}
